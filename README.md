# Laboratory

## SQL

Tools:
- MySQL (docker)
- MySQL Workbench

Run the "stack":
```bash
$ docker-compose up -d
```

Then open MySQL Workbench and setup a new connection:
- Hostname: `127.0.0.1`
- Port: `3386`
- Username: `root`
- Password: `example`

these are defaults, if you edit the docker-compose file, change connection
variables accordingly.
