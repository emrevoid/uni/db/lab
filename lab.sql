/* 1.	Visualizzare i clienti (customers) in ordine alfabetico */
/*select *
from Customers
order by CompanyName*/

/* 2.	Visualizzare i clienti che non hanno il fax */
/*select *
from Customers
where Fax is null*/

/* 3. Selezionare i nomi dei clienti (CompanyName) che iniziano con le lettere P, Q, R, S */
/*select CompanyName
from Customers
where CompanyName like 'P%'
     or CompanyName like 'Q%'
     or CompanyName like 'R%'
     or CompanyName like 'S%'*/
     
/*select CompanyName
from Customers
where CompanyName between 'P%' and 'T'*/

/*select CompanyName
from Customers
where LEFT(CompanyName, 1) IN ('P', 'Q', 'R', 'S')*/

/* 4.	Visualizzare Nome e Cognome degli impiegati assunti (HireDate) dopo il '1993-05-03' e aventi titolo di “Sales Representative” */
/*select FirstName, LastName
from Employees
where HireDate > '1993-05-03'
and Title = 'Sales Representative'*/

/* 5.    Selezionare la lista dei prodotti non sospesi (attributo discontinued), visualizzando
anche il nome della categoria di appartenenza*/
/*select P.ProductName, C.CategoryName
from Products P
join Categories C
	on P.CategoryID = C.CategoryID
where P.Discontinued = 0*/

/* CON JOIN IMPLICITO */
/*select P.ProductName, C.CategoryName
from Products P, Categories C
where P.CategoryID = C.CategoryID
and P.Discontinued = 0*/

/* 6. Selezionare gli ordini relativi al cliente ‘Ernst Handel’ (CompanyName)*/
/*select *
from Orders O
join Customers C
	on O.CustomerID = C.CustomerID
where C.CompanyName = 'Ernst Handel'*/

	/* CON QUERY INNESTATA */
/*select *
from Orders O
where O.CustomerID = (select CustomerID
						from Customers C
                        where C.CompanyName = 'Ernst Handel')*/

/* 7.	Selezionare il nome della società e il telefono dei corrieri (shippers) che hanno consegnato 
ordini nella città di ‘Rio de Janeiro’ (ShipCity in orders)
N.B. nella tabella orders l'id corriere è l'attributo ShipVia*/
/*select DISTINCT CompanyName, Phone
from Shippers S
join Orders O
	on S.ShipperID = O.ShipVia
where O.ShipCity = 'Rio de Janeiro'*/

/* 8.	Selezionare gli ordini (OrderID, OrderDate, ShippedDate) per cui la spedizione (ShippedDate)
è avvenuta entro 30 giorni dalla data dell’ordine (OrderDate)*/
/*select OrderID, OrderDate, ShippedDate, datediff(ShippedDate, OrderDate)
from Orders
where datediff(ShippedDate, OrderDate) <= 30*/

/* 9.	Selezionare l’elenco dei prodotti che hanno un costo compreso tra 18 e 50, ordinando il risultato
in ordine di prezzo crescente */
/*select *
from Products
where UnitPrice between 18 and 50
order by UnitPrice asc*/

/* 10.	Selezionare tutti i clienti (CustomerID, CompanyName) che hanno ordinato il prodotto 'Chang'*/
/*select DISTINCT C.CustomerID, C.CompanyName
from Customers C
join Orders O
	on C.CustomerID = O.CustomerID
join `Order Details` OD
	on O.OrderID = OD.OrderID
join Products P
	on OD.ProductID = P.ProductID
where ProductName ='Chang'*/


/* 11.	Selezionare i clienti che non hanno mai ordinato prodotti di categoria ‘Beverages’*/
-- ok anche con not exists (select * ...
-- 							and C.CustomerID = O.CustomerID
/*select CompanyName
from Customers
where CustomerID not in (select CustomerID
						from Orders O
						join `Order Details` OD
							on O.OrderID = OD.OrderID
						join Products P
							on OD.ProductID = P.ProductID
						join Categories C
							on P.CategoryID = C.CategoryID
						where C.CategoryName = 'Beverages')*/

/* 12.	Selezionare il prodotto più costoso*/
/*select *
from Products P
where P.UnitPrice = (select MAX(P.UnitPrice)
					from Products P)*/

/* 13.	Visualizzare l’importo totale di ciascun ordine fatto dal cliente 'Ernst Handel' (CompanyName)*/
/*select OD.OrderID, SUM(UnitPrice * Quantity) AS prezzo
from `Order Details` OD
join Orders O
	on O.OrderID = OD.OrderID
join Customers C
	on C.CustomerID = O.CustomerID
where C.CompanyName = 'Ernst Handel'
group by OD.OrderID*/

/* 14.	Selezionare il numero di ordini ricevuti in ciascun anno */
/*select year(OrderDate), count(OrderDate)
from Orders
group by year(OrderDate)*/

/* 15.	Visualizzare per ogni impiegato (EmployeeID, LastName, FirstName) il numero di clienti distinti serviti per ciascun paese (Country),
visualizzando il risultato in ordine di impiegato e di paese*/
/*
-- errata (conta su OrderID, ma un cliente può fare più ordini)
select DISTINCT E.EmployeeID, count(OrderID), ShipCountry, LastName, FirstName
from Orders O
join Employees E
	on O.EmployeeID = E.EmployeeID
group by E.EmployeeID, O.ShipCountry, LastName, FirstName
order by LastName, Country*/
/*
-- corretta
select E.EmployeeID, LastName, FirstName, C.Country, count(distinct C.CustomerID) as numCustomers
from Orders O
join Employees E
	on O.EmployeeID = E.EmployeeID
join Customers C
	on C.CustomerID = O.CustomerID
group by E.EmployeeID, LastName, FirstName, C.Country
order by LastName, Country*/
/*
-- prof
select E.EmployeeID, LastName, FirstName, C.Country, count(distinct C.CustomerID) as numCustomers
from Employees E, Orders O, Customers C
where E.EmployeeID = O.EmployeeID
and O.CustomerID = C.CustomerID
group by E.EmployeeID, LastName, FirstName, C.Country
order by 2,3,4
*/

/* 16.	Visualizzare per ogni corriere il numero di consegne effettuate, compresi i dati dei 
corrieri che non hanno effettuato nessuna consegna */
/*select ShipperID, CompanyName, count(ShipVia)
from Orders O
right outer join Shippers S
	on O.ShipVia = S.ShipperID
group by ShipperID, CompanyName*/

-- prof
/*select ShipperID, CompanyName, count(O.OrderID)
from Shippers S
left join Orders O
on S.ShipperID = O.ShipVia
group by ShipperID, CompanyName*/

/* 17.	Visualizzare i fornitori (SupplierID, CompanyName) che forniscono un solo prodotto */
/*select P.SupplierID, CompanyName, count(*)
from Suppliers S
join Products P
	on P.SupplierID = S.SupplierID
group by P.SupplierID, CompanyName
having count(P.SupplierID) = 1*/

/* 18.	Visualizzare tutti gli impiegati che sono stati assunti dopo Margaret Peacock */
/*select *
from Employees
where HireDate > (select HireDate
					from Employees
					where LastName = 'Peacock'
					and FirstName = 'Margaret')*/

/* 19.	Visualizzare gli ordini relativi al prodotto più costoso */
/*select *
from Orders O
join `Order Details` OD
	on OD.OrderID = O.OrderID
join Products P
	on OD.ProductID = P.ProductID
where OD.ProductID = (select ProductID
						from Products
						where UnitPrice = (select MAX(UnitPrice)
										   from Products))*/

-- prof
/*select *
from Orders O
join `Order Details` OD
	on OD.OrderID = O.OrderID
where OD.ProductID IN (select ProductID
						from Products
						where UnitPrice = (select MAX(UnitPrice)
										   from Products))*/
                                       
/* 20.	Visualizzare i nomi dei clienti per i quali l’ultimo ordine è relativo al 1998  */
/*select distinct ContactName
from Orders O, Customers C
where O.CustomerID = C.CustomerID
and year(O.OrderDate) = (select max(year(OrderDate))
						 from Orders O1
						 where O.CustomerID = O1.CustomerID)
and year(O.OrderDate) = 1998*/

-- prof
/*select C.CustomerID, CompanyName
from Customers C, Orders O
where C.CustomerID = O.CustomerID
group by C.CustomerID, CompanyName
having max(year(OrderDate)) = 1998*/

/* 21.	Contare il numero di clienti che non hanno effettuato ordini */
/*
select count(*)
from Customers C
left join Orders O
on C.CustomerID = O.CustomerID
where O.OrderID is null
*/

-- prof
/*
select count(*)
from Customers C
where CustomerID not in (select O.CustomerID
						 from Orders O)
*/
                         
/* 22.	Visualizzare il prezzo minimo, massimo e medio dei prodotti per ciascuna categoria */
/*
select C.CategoryID, C.CategoryName, min(round(UnitPrice, 2)), max(round(UnitPrice, 2)), round(avg(UnitPrice), 2)
from Products P, Categories C
where P.CategoryID = C.CategoryID
group by C.CategoryID, C.CategoryName
*/

/* 23.	Selezionare i prodotti che hanno un prezzo superiore al prezzo medio dei prodotti forniti dallo stesso fornitore */
/*
select P.ProductName
from Products P
where P.UnitPrice > (select avg(UnitPrice)
				   from Products P1
				   where P1.ProductID = P.ProductID
					)
*/

/* 24.	Visualizzare, in ordine decrescente rispetto alla quantità totale venduta, i prodotti che hanno venduto una quantità 
totale superiore al prodotto ‘Chai’ */
-- 828
/*
select OD.ProductID, sum(Quantity)
from `Order Details` OD
group by OD.ProductID
having sum(Quantity) > (select sum(Quantity)
						from `Order Details` OD, Products P
						where OD.ProductID = P.ProductID
						and ProductName = 'Chai'
						)
order by sum(Quantity) desc
*/

/* 25.	Visualizzare il nome dei clienti che hanno fatto almeno due ordini di importo superiore a 15000 */
/*with Orders15k as (
select O.CustomerID, O.OrderID
from Orders O, `Order Details` OD
where O.OrderID = OD.OrderID
group by O.OrderID
having sum(UnitPrice * Quantity) > 15000)

select CustomerID, count(*)
from Orders15k
group by CustomerID
having count(*) >= 2*/

-- prof
/*
select C.CompanyName
from Customers C, Orders O
where C.CustomerID = O.CustomerID
and OrderID in (select OD.OrderId
				from `Order Details` OD
                group by OD.OrderID
                having sum(OD.Quantity * OD.UnitPrice) > 15000)
group by C.CustomerID, C.CompanyName
having count(*) >= 2
*/
                
/* 26.	Individuare i codici dei clienti che hanno fatto un numero di ordini pari a quello del cliente 'Eastern Connection' */
-- prof
/*
select O.CustomerID
from Orders O
group by O.CustomerID
having count(*) = (select count(*)
					from Orders O1, Customers C
					where O1.CustomerID = C.CustomerID
					and C.CompanyName = 'Eastern Connection'
				   )
*/